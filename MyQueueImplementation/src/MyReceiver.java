import java.util.Properties;

import javax.jms.*;  
import javax.naming.InitialContext;  
  
public class MyReceiver {  
    public static void main(String[] args) {  
        try{  
            // pareil comme dans l'app Sender, on cr�e d'abord un nouveau contexte
        	InitialContext ctx = new InitialContext();  
        	// on ontient la connection factory cr�� sur glassfish console et on demarre la connexion
            QueueConnectionFactory f=(QueueConnectionFactory)ctx.lookup("myQueueConnectionFactory");  
            QueueConnection con=f.createQueueConnection();  
            con.start();  
            
            QueueSession ses=con.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);  
            // on obtient egalement la destionation resource myQueue cree sur glassfish console
            Queue t=(Queue)ctx.lookup("myQueue");  
            // on cree le receiver
            QueueReceiver receiver=ses.createReceiver(t);
            // on cr�e le listener 
            MyListener listener=new MyListener();    
            receiver.setMessageListener(listener);  
              
            System.out.println("Receiver est pret a ecouter ... ");
            while(true){                  
                Thread.sleep(1000);  
            }  
        }catch(Exception e){System.out.println(e);}  
    }  
  
}  