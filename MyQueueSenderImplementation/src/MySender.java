import java.io.BufferedReader;  
import java.io.InputStreamReader;  
import javax.naming.*;  
import javax.jms.*; 

public class MySender {
	
    public static void main(String[] args) {  
        try  
        {   // cr�ation de contexte
            InitialContext ctx = new InitialContext();  
            // factory pr�c�demment cr�� sur glassfish admin consol "myQueueConnectionFactory"
            QueueConnectionFactory f=(QueueConnectionFactory)ctx.lookup("myQueueConnectionFactory");  
            QueueConnection con=f.createQueueConnection();  
            con.start();  
            // cr�ation de queue pour la session 
            QueueSession ses=con.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);  
            //3) on obtient l'objet queue (JMS destionation resource) - pr�c�demment cr�� sur glassfish consol 
            Queue t=(Queue)ctx.lookup("myQueue");  
            // cr�ation de l'objet sender       
            QueueSender sender=ses.createSender(t);  
            // cr�ation du message
            TextMessage msg=ses.createTextMessage();  
            BufferedReader b=new BufferedReader(new InputStreamReader(System.in));  
            while(true)  
            {  
                System.out.println("Entrez le message:");  
                String s=b.readLine();  
                if (s.equals("end"))  
                    break;  
                msg.setText(s);
                // envoyer le message
                sender.send(msg);  
                System.out.println("Message envoy�.");  
            }  
            // cloturer la session
            con.close();  
        }catch(Exception e){System.out.println(e);}  
    }
}

